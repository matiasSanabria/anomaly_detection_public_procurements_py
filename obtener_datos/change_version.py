import os
import json


def change_version():
    indir = '/home/maeli/Documents/contratos_change_version/all/'
    for f in os.listdir(indir):
        try:
            with open(indir + f) as json_data:
                data = json.load(json_data)
                try:
                    # tender_id = data["records"][0]["compiledRelease"]["id"]
                    tender_id = data["records"][0]["versionedRelease"][0]["id"]
                    tender_id = tender_id[0:6]
                    indir2 = "/home/maeli/PycharmProjects/change_version_1_1_ocds/ofertantes/ofertantes/rename/"
                    try:
                        with open(indir2 + tender_id + '.json') as tenderers_json:
                            tenderers = json.load(tenderers_json)
                            num_tenderes = len(tenderers["@graph"][0]["proveedor"]["list"])
                            data["records"][0]["compiledRelease"]["tender"]["numberOfTenderers"] = num_tenderes
                            # data["records"][0]["versionedRelease"][0]["tender"]["numberOfTenderers"] = num_tenderes
                            tenderers_list = []
                            for k in range(0, num_tenderes):
                                name = tenderers["@graph"][0]["proveedor"]["list"][k]["razon_social"]
                                if tenderers["@graph"][0]["proveedor"]["list"][k]["ruc"] is not None:
                                    ruc = tenderers["@graph"][0]["proveedor"]["list"][k]["ruc"]
                                else:
                                    ruc = 'null'
                                tenderer = {}
                                identifier = {"legalName": name, "id": ruc}
                                tenderer["identifier"] = identifier
                                tenderer["name"] = name
                                tenderers_list.append(tenderer)

                            data["records"][0]["compiledRelease"]["tender"]["tenderers"] = tenderers_list
                            # data["records"][0]["versionedRelease"][0]["tender"]["tenderers"] = tenderers_list
                            with open('/home/maeli/Documents/contratos_change_version/a_validar/' + tender_id[:6] + '.json', 'w') as outfile:
                            # with open('/home/maeli/Documents/contratos_change_version/a_validar_versioned/' + tender_id[:6] + '.json', 'w') as outfile:
                                json.dump(data, outfile)
                                os.remove(indir + f)
                    except (ValueError, IOError):
                        with open('/home/maeli/Documents/contratos_change_version/no_existe_ofertantes/' + tender_id[:6] + '.json', 'w') as outfile:
                        # with open('/home/maeli/Documents/contratos_change_version/no_existe_ofertantes_versioned/' + tender_id[:6] + '.json', 'w') as outfile:
                            json.dump(data, outfile)
                            os.remove(indir + f)
                except KeyError:
                    with open('/home/maeli/Documents/contratos_change_version/no_tender/' + data["records"][0]["ocid"][12:] + '.json', 'w') as outfile:
                    # with open('/home/maeli/Documents/contratos_change_version/no_tender_versioned/' + data["records"][0]["ocid"][12:] + '.json', 'w') as outfile:
                        json.dump(data, outfile)
                        os.remove(indir + f)
        except ValueError:
            os.rename(indir + f, "/home/maeli/Documents/contratos_change_version/something_wrong/" + f)


change_version()
