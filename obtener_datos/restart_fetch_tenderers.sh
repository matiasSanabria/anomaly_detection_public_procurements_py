#!/bin/bash
sudo python3 fetch_releases_hacienda.py
echo "First crash, resume"
sleep 1
until sudo python3 fetch_releases_hacienda.py --resume; do
    echo "Process crashed. Restarting"
    sleep 1
done
