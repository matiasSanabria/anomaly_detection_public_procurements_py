import psycopg2
import json


def agregar_etiquetas():
    from psycopg2.extras import register_json
    register_json(oid=3802, array_oid=3807)
    query = """
    Select id, data from contratos;
    """
    # conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
    conn = psycopg2.connect(dbname='maeli', user='mekehler', password='Superhero')
    cur = conn.cursor()
    cur.execute(query)
    contratos = cur.fetchall()
    cur.close()
    del cur
    conn.close()
    del conn
    for row in contratos:
        try:
            data = row[1]
            conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
            cursor = conn.cursor()
            query = "SELECT data " \
                    "from etiquetas " \
                    "where id = " + str(row[0]) + ";"
            cursor.execute(query)
            eti = cursor.fetchone()
            cursor.close()
            del cursor
            conn.close()
            del conn
            NoneType = type(None)
            if not isinstance(eti, NoneType):
                etiqueta_file = eti[0]
                etiquetas = etiqueta_file["@graph"][0]["etiquetas"]
                print(etiquetas)
                data["records"][0]["compiledRelease"]["planning"]["etiquette"] = etiquetas
                data["extensions"] = ["https://gitlab.com/MaEliK/ocds_etiquette_extension/blob/master/extension.json", ]

            conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
            cursor = conn.cursor()
            query = "INSERT INTO contratos_con_etiqueta (id, data) " \
                    "VALUES (%s, %s);"
            d = (row[0], json.dumps(data, indent=2, ensure_ascii=False))
            cursor.execute(query, d)
            conn.commit()
            cursor.close()
            del cursor
            conn.close()
            del conn
        except Exception as e:
            with open("errors-agregar-etiqueta.txt", 'a') as err:
                print('Error: ' + str(e) + " " + str(row[0]))
                if str(e)[:13] != "duplicate key":
                    err.write(str(row[0]) + "\n")


agregar_etiquetas()
