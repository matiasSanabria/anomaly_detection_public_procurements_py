#!/bin/bash
sudo python3 fetch_py.py --all --year=$1
echo "First crash, resume"
sleep 1
until sudo python3 fetch_py.py --all --year=$1 --resume; do
    echo "Process crashed. Restarting"
    sleep 1
done
