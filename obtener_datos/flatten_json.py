import collections
import json
import psycopg2


def flattenPattern(pattern):
    newPattern = {}
    if type(pattern) is list:
        for val in pattern:
            pattern = val
            if type(pattern) is not str and type(pattern) is not int and type(pattern) is not list:
                if pattern is not None:
                    for key, value in pattern.items():
                        if type(value) in (list, dict):
                            returned_data = flattenPattern(value)
                            if type(returned_data) is str:
                                newPattern[key] = returned_data
                            else:
                                if returned_data is not None:
                                    for i, j in returned_data.items():
                                        newPattern[key + "." + i] = j
                        else:
                            newPattern[key] = value

    if type(pattern) is not str and type(pattern) is not int and type(pattern) is not list:
        if pattern is not None:
            for key, value in pattern.items():
                if type(value) in (list, dict):
                    returned_data = flattenPattern(value)
                    if type(returned_data) is str:
                        newPattern[key] = returned_data
                    else:
                        if returned_data is not None:
                            for i, j in returned_data.items():
                                newPattern[key + "." + i] = j
                else:
                    newPattern[key] = value
    elif type(pattern) is str:
        newPattern = pattern
    return newPattern


def getDataFromFile(fileName):
    with open(fileName, 'r') as json_file:
        data = json.load(json_file)
        return data


def getData():
    conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
    cursor = conn.cursor()
    query = "SELECT data " \
            "from contratos_v1_1_etiqueta_tenderers_hacienda limit 30000 offset 160000"
    cursor.execute(query)
    data = cursor.fetchall()
    cursor.close()
    del cursor
    conn.close()
    del conn
    pattern = []
    newnewkeys = getDataFromFile('/home/maeli/PycharmProjects/change_version_1_1_ocds/datos_flat.json')
    # row = data
    for row in data:
        pattern.append(flattenPattern(row[0]))
        keys = getDataFromFile('/home/maeli/PycharmProjects/change_version_1_1_ocds/datos_merged_flat3.json')
        new_keys = list(pattern[len(pattern) - 1].keys())
        for i in new_keys:
            if i not in keys and i not in newnewkeys:
                newnewkeys.append(i)
    with open('/home/maeli/PycharmProjects/change_version_1_1_ocds/datos_flat.json', 'w') as outfile:
        json.dump(newnewkeys, outfile)

getData()
# getDataFromFile()