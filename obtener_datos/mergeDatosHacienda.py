from jsonmerge import merge
from jsonmerge import Merger
import json
import psycopg2


def mergeDatosHacienda():
    from psycopg2.extras import register_json
    register_json(oid=3802, array_oid=3807)
    query = """
       Select id, data from contratos_v1_1_etiqueta_tenderers_2018;
       """
    conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
    cur = conn.cursor()
    cur.execute(query)
    contratos = cur.fetchall()
    cur.close()
    del cur
    conn.close()
    del conn
    # row = contratos
    for row in contratos:
        try:
            data_general = row[1]

            conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
            cursor = conn.cursor()
            query = "SELECT data " \
                    "from releases_hacienda " \
                    "where id = " + str(row[0]) + ";"
            cursor.execute(query)
            data_hacienda = cursor.fetchone()
            cursor.close()
            del cursor
            conn.close()
            del conn

            # append extensions
            extensions = []
            extensions.append('https://gitlab.com/MaEliK/ocds_etiquette_extension/raw/master/extension.json')
            extensions.append('https://raw.githubusercontent.com/open-contracting/api_extension/master/extension.json')
            data_general['extensions'] = extensions

            # eliminate validationErrors
            data_general.pop('validationErrors', None)

            if 'compiledRelease' in data_general['records'][0]:
                if 'etiquette' in data_general['records'][0]['compiledRelease']['planning']:
                    data_general['records'][0]['compiledRelease']['planning']['etiquettes'] = data_general['records'][0]['compiledRelease']['planning']['etiquette']
                    data_general['records'][0]['compiledRelease']['planning'].pop('etiquette', None)

                NoneType = type(None)
                if not isinstance(data_hacienda, NoneType):
                    data_hacienda = data_hacienda[0]
                    print('enter')
                    # uri hacienda agregar a listado de releases en record package
                    uri_hacienda = data_hacienda['uri']
                    # print(uri_hacienda)
                    data_general['records'][0]['releases'][0]['uri'] = uri_hacienda

                    # append extensions
                    for i in data_hacienda['extensions']:
                        data_general['extensions'].append(i)

                    record_package = data_general['records'][0]['compiledRelease']
                    hacienda = data_hacienda['releases'][0]

                    schema = {"mergeStrategy": "append"}
                    merger = Merger(schema)

                    # agregar packageMetadata
                    meta_data_hacienda = {}
                    meta_data_hacienda['uri'] = uri_hacienda
                    meta_data_hacienda['publishedDate'] = data_hacienda['publishedDate']
                    meta_data_hacienda['publisher'] = data_hacienda['publisher']
                    meta_data_hacienda['license'] = data_hacienda['license']
                    meta_data_hacienda['publicationPolicy'] = data_hacienda['publicationPolicy']
                    record_package['packageMetadata'] = meta_data_hacienda

                    # meta_data_record_package = {}
                    # meta_data_record_package['uri'] = data_general['uri']
                    # meta_data_record_package['publishedDate'] = data_general['publishedDate']
                    # meta_data_record_package['publisher'] = data_general['publisher']
                    # meta_data_record_package['license'] = data_general['license']
                    # meta_data_record_package['publicationPolicy'] = data_general['publicationPolicy']

                    # packageMetadata = []
                    # packageMetadata.append(meta_data_record_package)
                    # packageMetadata.append(meta_data_hacienda)
                    # record_package['packageMetadata'] = meta_data_record_package

                    record_package['id'] = merge(record_package['id'], hacienda['id'])
                    record_package['date'] = merge(record_package['date'], hacienda['date'])

                    # merge parties
                    parties = merger.merge(record_package['parties'], hacienda['parties'])
                    record_package['parties'] = parties

                    # merge planning , append documents
                    planning = merge(record_package['planning'], hacienda['planning'])
                    if ('documents' in record_package['planning']) and ('documents' in hacienda['planning']):
                        docs = merger.merge(record_package['planning']['documents'], hacienda['planning']['documents'])
                        planning['documents'] = docs
                    record_package['planning'] = planning

                    # contacts if id is the same merge, append documents, items,lots
                    contratos = []
                    for contrato_rp in record_package['contracts']:
                        contracts = {}
                        c = False
                        for contrato_hacienda in hacienda['contracts']:
                            if contrato_rp['id'] == contrato_hacienda['id']:
                                contracts = merge(contrato_rp, contrato_hacienda)
                                c = True
                                if ('documents' in contrato_rp) and ('documents' in contrato_hacienda):
                                    docs = merger.merge(contrato_rp['documents'], contrato_hacienda['documents'])
                                    contracts['documents'] = docs
                                if ('items' in contrato_rp) and ('items' in contrato_hacienda):
                                    docs = merger.merge(contrato_rp['items'], contrato_hacienda['items'])
                                    contracts['items'] = docs
                                if ('lots' in contrato_rp) and ('lots' in contrato_hacienda):
                                    docs = merger.merge(contrato_rp['lots'], contrato_hacienda['lots'])
                                    contracts['lots'] = docs
                                contratos.append(contracts)
                        if c is False:
                            contratos.append(contrato_rp)
                    for contrato_hacienda in hacienda['contracts']:
                        c = False
                        for contrato_rp in record_package['contracts']:
                            if contrato_rp['id'] == contrato_hacienda['id']:
                                c = True
                        if c is False:
                            contratos.append(contrato_hacienda)
                    record_package['contracts'] = contratos

                    # buyer merge
                    buyer = merge(record_package['buyer'], hacienda['buyer'])
                    record_package['buyer'] = buyer

                    # awards merge append suppliers
                    awards = []
                    for awards_rp in record_package['awards']:
                        c = False
                        award = {}
                        for awards_hacienda in hacienda['awards']:
                            if awards_rp['id'] == awards_hacienda['id']:
                                award = merge(awards_rp, awards_hacienda)
                                c = True
                                if ('suppliers' in awards_rp) and ('suppliers' in awards_hacienda):
                                    docs = merger.merge(awards_rp['suppliers'], awards_hacienda['suppliers'])
                                    award['suppliers'] = docs
                                awards.append(award)
                        if c is False:
                            awards.append(awards_rp)
                    for awards_hacienda in hacienda['awards']:
                        c = False
                        for awards_rp in record_package['awards']:
                            if awards_rp['id'] == awards_hacienda['id']:
                                c = True
                        if c is False:
                            awards.append(awards_hacienda)
                    record_package['awards'] = awards

                    # tender merge append lots, items, documents, tenderers,
                    tender = merge(record_package['tender'], hacienda['tender'])
                    if ('documents' in record_package['tender']) and ('documents' in hacienda['tender']):
                        docs = merger.merge(record_package['tender']['documents'], hacienda['tender']['documents'])
                        tender['documents'] = docs
                    if ('lots' in record_package['tender']) and ('lots' in hacienda['tender']):
                        docs = merger.merge(record_package['tender']['lots'], hacienda['tender']['lots'])
                        tender['lots'] = docs
                    if ('items' in record_package['tender']) and ('items' in hacienda['tender']):
                        docs = merger.merge(record_package['tender']['items'], hacienda['tender']['items'])
                        tender['items'] = docs
                    if ('tenderers' in record_package['tender']) and ('tenderers' in hacienda['tender']):
                        docs = merger.merge(record_package['tender']['tenderers'], hacienda['tender']['tenderers'])
                        tender['tenderers'] = docs
                    record_package['tender'] = tender

                    # tag
                    for i in hacienda['tag']:
                        record_package['tag'].append(i)

                    print('all appended')

                    data_general['records'][0]['compiledRelease'] = record_package

                # with open('/home/maeli/PycharmProjects/change_version_1_1_ocds/pruebas_datos/merged-test1.json', 'w') as outfile:
                #     json.dump(data_general, outfile)

            conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
            cursor = conn.cursor()
            query = "INSERT INTO contratos_v1_1_etiqueta_tenderers_hacienda_2018 (id, data) " \
                    "VALUES (%s, %s);"
            d = (row[0], json.dumps(data_general, indent=2, ensure_ascii=False))
            cursor.execute(query, d)
            conn.commit()
            cursor.close()
            del cursor
            conn.close()
            del conn
        except Exception as e:
            with open("errors-agregar-hacienda.txt", 'a') as err:
                print('Error: ' + str(e) + " " + str(row[0]))
                if str(e)[:13] != "duplicate key":
                    err.write(str(row[0]) + "\n")


mergeDatosHacienda()
