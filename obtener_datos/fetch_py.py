import optparse
import requests
import requests_cache
import json
import csv
from ratelimit import *
import psycopg2
import time

# Search, fetch records, fetch releases

year = 2016
error = 0
total = 0


@rate_limited(20)
def fetchData(id_list, folder, page=0):
    global year
    global error
    global total
    record_id = id_list[page]

    total = total + 1
    print("Fetching record " + str(page) + '/' + str(len(id_list)) + ' ID: ' + str(
        record_id) + " > https://www.contrataciones.gov.py:443/datos/api/v2/doc/ocds/record-package/" + str(record_id))
    r = requests.get('https://www.contrataciones.gov.py:443/datos/api/v2/doc/ocds/record-package/' + str(record_id))
    print(r)
    rr = r.status_code
    while rr == 429:
        print('sleep 10 seconds then retry')
        time.sleep(10)
        print("Fetching record " + str(page) + '/' + str(len(id_list)) + ' ID: ' + str(
            record_id) + " > https://www.contrataciones.gov.py:443/datos/api/v2/doc/ocds/record-package/" + str(
            record_id))
        r = requests.get('https://www.contrataciones.gov.py:443/datos/api/v2/doc/ocds/record-package/' + str(record_id))
        print(r)
        rr = r.status_code

    try:
        # conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
        conn = psycopg2.connect(dbname='maeli', user='mekehler', password='Superhero')
        cursor = conn.cursor()
        query = "INSERT INTO contratos (id, year, data) VALUES (%s, %s, %s);"
        data = (record_id, year, json.dumps(r.json(), indent=2, ensure_ascii=False))
        cursor.execute(query, data)
        conn.commit()
        cursor.close()
        del cursor
        conn.close()
        del conn

    # with open(folder + '/'+ str(record_id) +".json","w") as release:
    # release.write(json.dumps(r.json(),indent=2,ensure_ascii=False))
    except Exception as e:
        error = error + 1
        with open("errors" + str(year) + ".txt", 'a') as err:
            print('Error: ' + str(e))
            if str(e)[:13] != "duplicate key" and rr != 500 and rr != 404:
                err.write(str(record_id) + "\n")

    page = int(page) + 1
    with open("page.n", 'w') as n:
        n.write(str(page))

    if page < len(id_list):
        fetchData(id_list, folder, page)
    else:
        with open("page.n", 'w') as n:
            n.write("0")


@rate_limited(1)
def fetchList(year):
    print("Fetching " + str(year) + " listing")
    r = requests.get('https://www.contrataciones.gov.py/images/opendata/planificaciones/' + str(year) + '.csv')
    print("Parsing")
    decoded_content = r.content.decode('utf-8')
    cr = csv.reader(decoded_content.splitlines(), delimiter=',')

    id_list = []
    for row in list(cr):
        print(row[2])
        id_list.append(row[2])

    return id_list[1:]


def fetch_err_list(year):
    file_name = "errors" + str(year) + "err.txt"
    print("Fetching " + str(file_name) + " listing")
    id_list = []
    f = open(file_name, 'r')
    for line in f:
        print(str(line).rstrip())
        id_list.append(line.rstrip())
    return id_list[0:]


def main():
    global error
    global total
    global year
    requests_cache.install_cache('test_cache', backend='sqlite', expire_after=300)

    usage = 'Usage: %prog [ --all --cont ]'
    parser = optparse.OptionParser(usage=usage)

    parser.add_option('-a', '--all', action='store_true', default=True,
                      help='Fetch all records, rather than a small extract')

    parser.add_option('-R', '--resume', action='store_true', default=False,
                      help='Continue from the last page (in page.n), for when download broken')

    parser.add_option('-y', '--year', action='store', type="int", default=2019,
                      help='Which year to fetch activities from')

    parser.add_option('-e', '--error', action='store_true', default=False,
                      help='Retry items saved in error.txt file')

    (options, args) = parser.parse_args()

    if options.error:
        id_list = fetch_err_list(options.year)
    else:
        id_list = fetchList(options.year)

    year = options.year

    # id_list = [273637,273638,273639]
    if options.resume:
        with open("page.n", 'r') as n:
            page = n.read()
    else:
        page = 0

    if options.all:
        fetchData(id_list, 'all', int(page))
        print(str(error) + "/" + str(total))


if __name__ == '__main__':
    main()
