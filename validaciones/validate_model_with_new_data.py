from joblib import dump, load
from copy import deepcopy
import pandas as pd
from sklearn import preprocessing
import os
import re

# direccion = 'models/tender_written_training_6/'
# set_direccion = 'iforest_results/tender_written_training_6_protestas/'
#
# textfile = "../convert_data/convertedData/tender_written_training_6_protestas.csv"


# validate_with_new_data(direccion, set_direccion, textfile)


def validate_with_new_data(filename, textfile, sep):
    input_new_data = pd.read_csv(textfile, sep=sep)
    # drop id and ocid, tender_id (2, 6), award_id(28), contract_id(2, 34) y adenda_id (18)
    if 'tender' in textfile:
        new_data = input_new_data.drop(['id', 'ocid', 'tender_id'], axis=1)
    elif 'adenda' in textfile:
        new_data = input_new_data.drop(['id', 'ocid', 'contract_id', 'adenda_id'], axis=1)
        new_data = new_data.drop(['contract_supplier_roles', 'contract_status', 'adenda_contract_amount_increase'], axis=1)
    else:
        # new_data = input_new_data.drop(input_new_data.columns[[0, 1, 6, 28, 30, 34]], axis=1)
        new_data = input_new_data.drop(['id', 'ocid', 'tender_id', 'award_id', 'contract_id'], axis=1)
        new_data = new_data.drop(['contract_supplier_roles', 'tender_status', 'award_status', 'contract_status'], axis=1)

    # print(new_data.columns)
    new_data.replace(to_replace='None', value=0, inplace=True)
    new_data.replace(to_replace='NaN', value=0, inplace=True)
    new_data.fillna(0)
    new_data.dropna(inplace=True)

    clf = load(filename)
    # print(clf)
    prediction = clf.predict(new_data)

    # 1 is inlier, -1 outlier
    scores = clf.decision_function(new_data)

    predictions = pd.DataFrame()
    predictions["predicted_class"] = prediction
    predictions["scores"] = scores
    predictions["id"] = input_new_data["id"]
    if 'tender' in textfile or 'contract' in textfile or 'contratos' in textfile:
        predictions["tender_id"] = input_new_data["tender_id"]
    if 'contract' in textfile or 'contratos' in textfile:
        predictions["contract_id"] = input_new_data["contract_id"]
    if 'adenda' in textfile:
        if 'adenda_amount' in input_new_data.columns:
            predictions['adenda_amount'] = input_new_data['adenda_amount']
        predictions["adenda_contract_id"] = input_new_data["adenda_id"]

    # print(len(predictions))
    # print(predictions)
    # predictions.to_csv(set_direccion +
    #                    filename[:-7] + '.csv',
    #                    header=['Predicted Class',
    #                            'Scores',
    #                            'OCDS ID',
    #                            'Tender Id',
    #                            # 'Contract Id',
    #                            # 'Adenda Id'
    #                            ], index=None, sep=',', mode='w')
    #
    # print(predictions)
    return predictions
