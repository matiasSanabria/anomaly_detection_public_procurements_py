import hashlib
import codecs
import convert_data.diccionario as diccionario


def convertArray(value, dict_name):
    val = []
    for i in diccionario.dictionary[dict_name]:
        if i in value:
            val.append(1)
        else:
            val.append(0)
    i = 0
    for bit in val:
        i = (i << 1) | bit
    return i


def hashValue(text, name, hash):
    if text:
        if hash:
            hash_md5 = hashlib.md5(text.encode('utf-8'))
            hex_digest = hash_md5.hexdigest()
            key = int(hex_digest, 16)
            key = int(str(key)[0:18])
        else:
            key = text
        # if len(str(key)) > 32:
        #     print('hash_md5: ' + str(len(str(hash_md5))))
        #     print('hex_digest: ' + str(len(hex_digest)))
        #     print('key: ' + str(len(str(key))))
        # else:
        #     hex_codec = codecs.encode(text.encode('utf-8'), 'hex_codec')
        #     key = int(hex_codec, 16)
        # if len(str(key)) > 32:
        #     print('hex_codec: ' + str(len(hex_codec)))
        #     print('key: ' + str(len(str(key))))
        if name:
            if key in diccionario.name_diccionario.keys() & name != diccionario.name_diccionario[key]:
                raise Exception('value in dict already exists, old value: ' + diccionario.name_diccionario[
                    key] + ' newValue: ' + name)
            elif key not in diccionario.name_diccionario.keys():
                diccionario.name_diccionario[key] = name
        else:
            if key in diccionario.name_diccionario.keys() & text != diccionario.name_diccionario[key]:
                raise Exception('value in dict already exists, old value: ' + diccionario.name_diccionario[
                    key] + ' newValue: ' + text)
            elif key not in diccionario.name_diccionario.keys():
                diccionario.name_diccionario[key] = text
    else:
        key = None
    return key


def removeDividingCharacters(text):
    new = ''.join(c for c in text if c.isdigit())
    if new == '':
        new = None
    else:
        new = int(new)
    return new


def appendNameValue(var, name, title_row, row, test):
    try:
        indice = title_row.index(name)
    except ValueError:
        indice = -1
    if name == 'adenda_contract_dncpAmendmentType' and indice >= 0 and len(title_row) == len(row):
        print('is adenda contract dncpAmendmentType')
        row[indice] = var
    else:
        if indice < 0:
            title_row.append(name)
            row.append(var)
        else:
            if indice < len(row):
                if row[indice] is None and len(row) == indice:
                    row[indice] = var
                else:
                    row.insert(indice, var)
                if name not in title_row:
                    title_row.insert(indice, name)
            else:
                row.insert(indice, var)
            if name not in title_row:
                title_row.insert(indice, name)


def append_to_list(title_row, list_data, indice, funcion):
    for i in list_data.keys():
        if len(list_data[i]) < len(title_row):
            if funcion == 'append':
                list_data[i].append(None)
            elif funcion == 'insert':
                list_data[i].insert(indice, None)


def depth(d, level=1):
    if not isinstance(d, dict) or not d:
        return level
    return max(depth(d[k], level + 1) for k in d)