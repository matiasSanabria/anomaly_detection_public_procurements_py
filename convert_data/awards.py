from convert_data.getExchangeRate import getExchangeRate
from convert_data.convert_string import convertArray
import convert_data.parties as parties
from datetime import datetime
from convert_data.convert_string import appendNameValue
from convert_data.convert_string import hashValue


def awards(award, title_row, row, tender_end_date):
    award_id = award['id']
    moneda_date = None
    moneda = None
    amount = None
    award_date = None
    award_suppliers = None
    if award_id:
        appendNameValue(award_id, 'award_id', title_row, row, None)
    if 'date' in award:
        date = datetime.strptime(award['date'], '%Y-%m-%dT%H:%M:%SZ')
        month = date.month
        moneda_date = date
        award_date = date.strftime('%s')
        appendNameValue(int(month), 'award_dateMonth', title_row, row, None)
    if 'value' in award:
        moneda = award["value"]["currency"]
        amount = award["value"]["amount"]
    if moneda is not None and amount is not None:
        exchange_rate = getExchangeRate(moneda, moneda_date)
        planning_budget_amount = amount * exchange_rate
        appendNameValue(planning_budget_amount, 'award_amount', title_row, row, None)
    if 'status' in award:
        award_status = convertArray(award['status'], 'status')
        appendNameValue(award_status, 'award_status', title_row, row, None)
    if type(tender_end_date) is str and type(award_date) is str:
        period = int(award_date) - int(tender_end_date)
        appendNameValue(int(period), 'period_tender_award', title_row, row, None)
    if 'suppliers' in award:
        award_suppliers = award['suppliers']
    return [award_id, title_row, row, moneda_date, award_suppliers]
