import psycopg2


def getExchangeRate(moneda, date):
    if moneda == 'USD':
        date = date.date()
        print(date)
        conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
        cursor = conn.cursor()
        query = "SELECT compra from cambios_dolar_gs_bnf where fecha = %s;"
        variables = [str(date)]
        cursor.execute(query, variables)
        datos = cursor.fetchone()
        cursor.close()
        del cursor
        conn.close()
        del conn
        return datos[0]
        # return 6000
    elif moneda == 'PYG':
        return 1
