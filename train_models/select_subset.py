import pandas as pd
import os
import json
import numpy as np
import re

name = 'tender_written'
version = ''

textfile = "../convert_data/convertedData/converted_" + name + ".csv"
input_contract_data = pd.read_csv(textfile, sep=';')
print(input_contract_data)
original_input_contract_data = input_contract_data.copy(deep=True)

df1_percent = original_input_contract_data.sample(frac=0.1)
print(df1_percent.index)

for (index, value) in df1_percent['id'].iteritems():
    original_input_contract_data.drop(index, inplace=True)

original_input_contract_data.to_csv('../convert_data/convertedData/converted_' + name + '_training_set' + version + '.csv',
                           header=original_input_contract_data.columns, index=None, sep=',', mode='w')

df1_percent.to_csv('../convert_data/convertedData/converted_' + name + '_validation_set' + version + '.csv',
                   header=df1_percent.columns, index=None, sep=',', mode='w')

