-------------------------------------------------------------------------------
Obtener los datos
-------------------------------------------------------------------------------
1. Obtener datos en forma de record-package de la API proporcionada del DNCP
mediante el script fetch_py, el cual guarda en una base de datos PostgreSQL
creada anteriormente en formato jsonb.

2. Obtener las etiquetas de cada record-package mediante el script
fetch_etiquetas.py, se adjuntan a los datos del record-package con el script
agregar_etiquetas.py.

3. De la misma manera obtener los tenderers de cada record-package mediante el
script fetch_tenderers.py, y adjuntarlos al record-package con add_tenderers.py

4. Obtener las cotizaciones de bcp en forma de excel (descargadas en la carpeta
cotizaciones) y agregarlas a la base de datos con el script cotizaciones_a_DB.py

5. Descargar los datos de la base de datos a una carpeta local a fin de aplicar
el script del cambio de versión a la versión 1.1 del OCDS, utilizar el script
save_to_directory.py

6. Cambiar los datos a la versión 1.1 ejecutando el script update_to_v1_1.py

7. Validando el cambio de los datos mediante el script validate_v1_1.py

-------------------------------------------------------------------------------
Transformar los datos
-------------------------------------------------------------------------------
La transformación de los datos se realiza con 4 conjuntos de datos:

1. Información de planificación y tender, con estados activo y forma de entrega
"written", se ejecuta la función main_tender.py, cambiando en la consulta a la
base de datos el parametro de la forma de entrega y en las líneas 46 y 47 el
nombre de los archivos de salida csv y del diccionario. Si cae el proceso se puede
reanundar de vuelta, y añade a los archivos especificados.

2. Información de planificación y tender, con estados activo y forma de entrega
"electronicAuction", se ejecuta de la misma manera que el punto anterior.

3. Información de planificación, tender, award y contrato, con estado no activo y
forma de entrega "written", se ejecuta la función main_contracts.py, cambiando en la
consulta a la base de datos el parametro de la forma de entrega y en las líneas 61 y 62 el
nombre de los archivos de salida csv y del diccionario. Si cae el proceso se puede
reanundar de vuelta, y añade a los archivos especificados.

4. Información de planificación, tender, award y contrato, con estado no activo y
forma de entrega "electronicAuction", se ejecuta de la misma manera que el punto anterior.

5. Información de contrato y adendas, con estado no activo y forma de entrega "written",
se ejecuta la función main_adendas.py, cambiando en la consulta a la
base de datos el parametro de la forma de entrega y en las líneas 52 y 53 el
nombre de los archivos de salida csv y del diccionario. Si cae el proceso se puede
reanundar de vuelta, y añade a los archivos especificados.

4. Información de contrato y adenda, con estado no activo y forma de entrega
"electronicAuction", se ejecuta de la misma manera que el punto anterior.

Los scripts se encuentran en la carpeta convert_data

-------------------------------------------------------------------------------
Entrenar el modelo
-------------------------------------------------------------------------------
Para entrenar el modelo se utiliza el script scikit_isolation_forest.py en
train_models, se tiene que especificar el archivo con los datos convertidos, el número
de estimadores, el número de muestras y el número máximo de atributos a utilizar. El
script deja como resultado el modelo entrenado en un archivo joblib y los scores resultantes
para el conjunto de entrenamiento.

-------------------------------------------------------------------------------
Validar el modelo
-------------------------------------------------------------------------------
La validación se hace con dos conjuntos de datos conocidos cómo denuncias y protestas.
Se realiza con el script validaciones_protestas_denuncias.py.
